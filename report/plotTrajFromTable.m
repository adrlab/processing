function [ figs ] = plotTrajFromTable( trajtable )
%plotTrajFromTable Plot multiple trajectories and metadata stored in table.
%   Plots timeseries objects parsed in table trajtable along with
%   the metadata stored in the same table row. Trajectories and metadata is
%   identified according to variable names which must be consistent with
%   the format defined in MATDOC.
%   Returns handles to figures in a struct with the same fieldnames for the
%   trajectories as in trajtable.
%   All rows are considered.
%   For supported variable names see below.

validateattributes(trajtable, {'table'}, {'2d', 'nonempty'}, mfilename, 'trajtable');


variables = trajtable.Properties.VariableNames;

% Define table variable which contain the trajectories to plot.
[isTraj, ~] = ismember(variables, ...
    {'C_pos', 'C_ori', 'C_lac', 'C_aac', ...
    'I_lac', 'I_aac', 'F_for', 'F_tor'});

if sum(isTraj) == 0
    error('No trajectory found or variable names inconsistend. See MATDOC for format definition.');
else
    trajQuerry = variables(isTraj); % trajectory names found in table
    
    % TO DO Check that elements are timeseries objects.
    
%     figs = struct.empty(0, height(trajtable));
    figs = zeros(height(trajtable), sum(isTraj)); % store figure handles 
end

for i = 1:height(trajtable)
    for j = 1:length(trajQuerry)
        
        trajName = trajQuerry{j};
        
        ts = trajtable{i, trajName}; % timeseries object
        
        % Zoom into time interval around impact
        if sum(strcmp('IntervalsOfInterest', variables)) == 1
            interval = trajtable{i, 'IntervalsOfInterest'};
            tsZoom = ts.getsampleusingtime(interval(1), interval(2));
            tsZoom.Name = ts.Name;
        else
            % Define zoom interval as 0.5 sec before and after impact
            if isempty(ts.Events)
                tsZoom = ts;
            else
                interval = [ts.Events.Time-0.5, ts.Events.Time+0.5];
                tsZoom = ts.getsampleusingtime(interval(1), interval(2));
                tsZoom.Name = ts.Name;
            end
        end
    
        % Create plot
        figs(i, j) = figure;
        tsZoom.plot; %('Child', axes(figs(i).(trajName{:})));

        % Write Hit id in title
        if sum(strcmp('HitID', variables)) == 1
            title(sprintf('Hit No %u, %s', trajtable{i, 'HitID'}, trajName));
        else
            % don't change title
        end
        
        % Add legend
        legend('x', 'y', 'z');
        
        % Make figure wider
        pos = get(gcf, 'Position');
        set(gcf, 'Position', pos+[0 0 pos(3)*2 0]);
    end
end

end

