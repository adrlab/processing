# Data processing of spear trajectories

This repos contains the matlab code used to process the spear trajectories of the Neumark Nord replicative experiments conducted in December 2016. Most relevant steps are documented in import/preprocess.m and kinetics.m. 

This matlab code is licensed under the Apache2 license (see LICENSE.txt).
