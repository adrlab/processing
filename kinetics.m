%kinetics Compute kinetic quantities from preprocessed trajectories.
%   Computed Quantities are:
%   - Events in time series
%   - Velocities derived from position and acceleration
%   - Energy transferred into target
%   - RPY Angles of the spear
%   
% First detect impact in acceleration trajectory and standstill in position
% trajectory. Use these boundaries to integrate acceleration up to impact.
% Save plots into report so process steps can be checked.

clear all; close all;

% Report target file
reportFile = 'report/out/Kinetics_tmp';

% Load dataset:
DATA = load('data/dataset05.mat');

data = outerjoin(DATA.TRAJ, DATA.PARA, 'keys', 'ExpName', ...
    'RightVariables', ...
    {'ImpactID', 'Target', 'Modus', 'Participant'});

% Take out invalid rows
data = data(~logical(sum(ismissing(data), 2)) , :);

% Select experiments
Trajectories = sortrows(data(:,:), {'Modus', 'ExpName'});

Trajectories = Trajectories(:,:);

global counter
figReportSize = [200 100 1400 500];

KinTable = Trajectories(:, {'ImpactID', 'ExpName'});
KinTable.SpeedAtImpact      = NaN(height(Trajectories), 1);
KinTable.VelocityAtImpact   = NaN(height(Trajectories), 3);
KinTable.VelocityAtImpactPos = NaN(height(Trajectories), 3);
KinTable.SpeedAtImpactPos   = NaN(height(Trajectories), 1);
KinTable.WorkOnTarget       = NaN(height(Trajectories), 1);
KinTable.ImpactAngles       = NaN(height(Trajectories), 3);
KinTable.Inclination        = NaN(height(Trajectories), 1);
KinTable.Declination        = NaN(height(Trajectories), 1);
KinTable.PeakForceZ         = NaN(height(Trajectories), 1);
KinTable.WorkRelError       = NaN(height(Trajectories), 1);
KinTable{:, 'EndOfMotion'}  = tsdata.event;

KinTable.Properties.Description = 'Table of kinetic quantities computed from sensor data';
KinTable.Properties.VariableDescriptions(3:end) = { ...
    '2 norm of velocity at impact computed by integrating acceleration', ...
    'Velocity at impact computed by integrating acceleration', ...
    'Velocity at Impact computed by deriving position estimate', ...
    '2 norm of velocity at impact computed by deriving position estimate', ...
    'Work applied to target computed by integrating the force-displacement product', ...
    'Roll Pitch Yaw angles at impact between spear reference frame and tip frame', ...
    'Angle between the vertical plane of the target and the spear haft', ...
    'Angle between the horizontal plane and the spear haft', ...
    'Maximum force in z direction expressed in force frame', ...
    'Relative velocity offset at end of motion. Used for evaluating quality of work estimate.', ...
    'End of motion event, point in time after impact when spear is not moving anymore.'};

KinTable.Properties.VariableUnits = { '', '', ...
    'm/sec', 'm/sec', 'm/sec', 'm/sec', 'J', 'deg', 'deg', 'deg', 'N', '', 'sec'};



%% Initialize loop
import mlreportgen.dom.*

% Set-up new report
report = Document(reportFile, 'html');
report.StreamOutput = true;
report.open;

p = Paragraph('Trajcetories - All');
p.Style = {OuterMargin('0.5in','0in','0in','14pt')};
report.append(p);

report.append(Text(['Creation date: ', date]));

% Table of contents
toc = append(report, TOC(3,' '));
toc.Style = {PageBreakBefore(true)};

% reset counter for addFigures()
counter = 1;

fig1 = figure('position', figReportSize);
fig2 = figure('position', figReportSize);
fig3 = figure('position', figReportSize);
fig4 = figure('position', figReportSize);
fig5 = figure('position', figReportSize);
fig6 = figure('position', figReportSize);
fig7 = figure('position', figReportSize);


figHead1 = figure('position', figReportSize); 
figHead2 = figure('position', figReportSize); 
figHead3 = figure('position', figReportSize); 

% Load constant transforms 
run data/frames.m   % from CAD model
A_FC = A_FL * transpose(A_CL);
A_TF = A_TL * transpose(A_FL);
A_TU = A_TL * transpose(A_UL);
A_FU = A_FL * transpose(A_UL);

% Spear reference frame horizontal (Sh): Aligned with 
% the reference tag frame/ world frame, but swapped z and x axis.
A_ShW = [0 0 1; 0 1 0; -1 0 0];

% Spear reference frame vertical (Sh): Aligned with 
% the reference tag frame/ world frame, but rotated about pi around y
A_SvW = [-1 0 0; 0 1 0; 0 0 -1];

U_ov_F = (A_UL * ( - L_ov_U + L_ov_F )' )';
U_ov_L = (A_UL * (- L_ov_U )' )';

% Set constant distance between camera frame origin and f frame origin
C_ov_F = [-0.05 -0.1 0]; % in meter


for i=1:height(Trajectories)

    % Recreate axes
    ax1 = axes('parent', fig1); hold(ax1,'on');
    ax2 = axes('parent', fig2); hold(ax2,'on');
    ax3 = axes('parent', fig3); hold(ax3,'on');
    ax4 = axes('parent', fig4); hold(ax4,'on');
    ax5 = axes('parent', fig5); hold(ax5,'on');
    ax6 = axes('parent', fig6); hold(ax6,'on');
    ax7 = axes('parent', fig7); hold(ax7,'on');

    axHead1 = axes('parent', figHead1);
    axHead2 = axes('parent', figHead2);    
    axHead3 = axes('parent', figHead3);

    switch Trajectories.ExpName(i)
        case 'BP102_1'
            fprintf('Caution %s; Invalid data', 'BP102_1');
    end
    
    fprintf('Experiment %s\n', Trajectories.ExpName(i));
    
    
    % Get trajectories of acc, pose and force
    U_a_o   = Trajectories.U_a_o(i);    
    W_ov_U  = Trajectories.W_ov_U(i);   
    Q_WU    = Trajectories.Q_WU(i);
    F_F_o   = Trajectories.F_F_o(i);
    
    
    % Camer to IMU Tranfom from rcars estimator
    A_CU = quat2rotm(quatinv(Trajectories{i, 'Q_UC'}));
    U_ov_C = Trajectories{i, 'U_ov_C'};
    
    % Initilize ROS message for visualization with spear pose
    tfmsg = appendTransform(timeseries, 'world', 'U', W_ov_U, Q_WU);
    
    % Store Peak Forces in F-frame
    KinTable{i, 'PeakForceZ'} = max(abs(F_F_o.Data(:,3)));
    
    % Plot the unprocessed data
    plotTimeseriesSync([W_ov_U, F_F_o, U_a_o], 'parent', figHead1); 

    eventStandstill = Trajectories.Standstill(i);
    eventImpact     = Trajectories.Impact(i);
 
    
    axHead1 = findobj(figHead1, 'type', 'axes');
    legend(axHead1(1), 'show');
    legend(axHead1(3), 'show');
    
    
    annotateEvent(axHead1(1), eventImpact, 'line');
    annotateEvent(axHead1(2), eventImpact, 'line');
    annotateEvent(axHead1(3), eventImpact, 'line');
    figHead1.Position(4) = figHead1.Position(4) * 3;  
 
    
    
    %% ALIGN TIMES OF POSE AND IMU DATA 
    % First, detect impact in position trajectory:
    % Take peak neg velocity in x for horizontal and z for vertical
    % W_v_oU: Velocity at origin of IMU frame expressed in world frame.
    W_v_oU = timeseries( ...
        [gradient(W_ov_U.Data(:,1), W_ov_U.Time), ...
        gradient(W_ov_U.Data(:,2), W_ov_U.Time), ...
        gradient(W_ov_U.Data(:,3), W_ov_U.Time)], W_ov_U.Time, ...
        'Name', 'Velocity From Position');
    
    % Show in plot
    annotateEvent(axHead1(1), eventStandstill, 'line');
    
    plotTimeseriesSync([W_ov_U, U_a_o], 'parent', fig5); 
    ax5 = findobj(fig5, 'type', 'axes');
    annotateEvent(ax5(1), eventImpact, 'line');
    annotateEvent(ax5(2), eventStandstill, 'line');
    set(ax5(1), 'xlim', [eventImpact.Time-1 eventImpact.Time+1]);
    set(ax5(2), 'xlim', [eventImpact.Time-1 eventImpact.Time+1]);

    
    %% DETECT TURNING POINT 
    % Defined as the max elongation in x for horizontal modu/ z for
    % vertical modus during reach back phase
    if Trajectories{i, 'Modus'} == 'horizontal'
        [~, idxTurning] = max(W_ov_U.Data(:,1));
    elseif Trajectories{i, 'Modus'} == 'vertikal'
        [~, idxTurning] = max(W_ov_U.Data(:,3));
    else
        error('')
    end
    eventTurning = tsdata.event('Turning Point', W_ov_U.Time(idxTurning));

    annotateEvent(ax5(2), eventTurning, 'line');
    
    
    %% GRAVITY COMPENSATION AND VELOCITIES
    try
        
    % Crop and interpolate time vectors of IMU data to match time vector pose data
    [U_a_o, Q_WU] = synchronize(U_a_o, Q_WU, 'uniform', 'interval', mean(diff(Q_WU.Time)));
        
    W_g = -[0 0 9.81]; % Constant gravitational acceleration
    U_g = NaN(Q_WU.Length, 3); % gravity in IMU frame
    
    % gravity in IMU frame = rotation matrix * gravity in world
    for j=1:Q_WU.Length
         U_g(j, :) = quat2rotm(quatinv(Q_WU.Data(j,:))) * W_g';
    end

    % Substract gravity from IMU measurements
    % TO DO Check this choice of gravity components. Its justified based on visual
    % inspection. Don't know where the mistake is. Probably its the
    % difference between frame conventions of rcars and IMU factory
    % definition.
    % U_ax = U_gy
    % U_ay = U_gx
    % U_az = U_gz
    U_a_o_free = timeseries(U_a_o.Data + [U_g(:,1) U_g(:,2) U_g(:,3)], U_a_o.Time, 'Name', 'Free acceleration');
    

    if Trajectories{i, 'Modus'} == 'horizontal'
        % Transform free acceleration back to world frame
        W_a_o_free = timeseries;
        W_a_o_free.Name = 'Free acceleration in world';
        for j=1:Q_WU.Length
            W_a_o_free = W_a_o_free.addsample('Data', ...
                ( quat2rotm(Q_WU.Data(j,:)) * U_a_o_free.Data(j,:)' )' , ...
                'Time', Q_WU.Time(j));
        end

    elseif Trajectories{i, 'Modus'} == 'vertikal'
        % Transform acceleration into world frame and substract gracity
        W_a_o = timeseries;
        W_a_o.Name = 'Acceleration in world';
        for j=1:Q_WU.Length
            W_a_o = W_a_o.addsample('Data', ...
                ( quat2rotm(Q_WU.Data(j,:)) * U_a_o.Data(j,:)' )' , ...
                'Time', Q_WU.Time(j));
        end

        W_a_o_free = timeseries;
        W_a_o_free.Name = 'Free acceleration in world';
        W_a_o_free.Time = W_a_o.Time;
        W_a_o_free.Data = W_a_o.Data + W_g; % Remove gravity
    end
    
    
    % Plot free acceleration in world frame
    cla(ax5(1));
    plot(W_a_o_free, 'parent', ax5(1)); title(ax5(1), W_a_o_free.Name);
    annotateEvent(ax5(1), eventImpact, 'line');

    
    plot(W_a_o_free.gettsbetweenevents(eventStandstill, eventImpact), 'parent', ax2);
    title(ax2, 'Free accleration in world from standstill to impact'); grid(ax2, 'on'); 
    legend(ax2, 'show');
   
    
    % For velocity at impact, use only acceleration phase from turning
    % point to impact. Integrate acc in world frame!
    accAccPhase = W_a_o_free.gettsbetweenevents(eventTurning, eventImpact);
    
    plot(accAccPhase, 'parent', ax3); grid(ax3, 'on');

    velFromAcc = timeseries(cumtrapz(accAccPhase.Time, accAccPhase.Data), accAccPhase.Time);
    speedFromAcc = timeseries(rssq(velFromAcc.Data, 2), velFromAcc.Time, 'Name', 'Speed From Acceleration');
    
    plot(velFromAcc, 'Parent', ax4);
    plot(speedFromAcc, '.', 'Parent', ax4);
    grid(ax4, 'on');
        
    plot(W_v_oU.gettsbetweenevents(eventTurning, eventImpact), '--', 'Parent', ax4);
    
    % Store velocities at impact
    KinTable{i, 'VelocityAtImpact'} = velFromAcc.Data(end, :);
    KinTable{i, 'SpeedAtImpact'} = speedFromAcc.Data(end);
    KinTable{i, 'VelocityAtImpactPos'} = W_v_oU.gettsatevent(eventImpact).Data;
    KinTable{i, 'SpeedAtImpactPos'} = rssq(W_v_oU.gettsatevent(eventImpact).Data);
    
    catch ME
        report.append(Text(sprintf('Velocity computation failed: %s', ME.message)));
%         keyboard
    end

    
    
    %% ENERGY INTO TARGET 
    accOffset = nan(1,3);
    
    try
        % Compute energy transferred into target from force
        % Use formula for mechanical work: E = integral F(t)*v(t) dt

        % Determine impact event in force trajectory as done for acceleration
        idxImpactForce = find(gradient(rssq(F_F_o.Data, 2), F_F_o.Time) > 1e+4, 1, 'first');
        eventImpactForce = tsdata.event('Impact From Force', F_F_o.Time(idxImpactForce));

        % Align times of F/T sensor and pose estimates using impact event 
        deltaT = F_F_o.Time(idxImpactForce) - eventImpact.Time;

        F_F_o.Time = F_F_o.Time - deltaT;
        eventImpactForce.Time = eventImpactForce.Time - deltaT;


        % Detect end of oscilation event using MATLABs feature detector, findchangepts 
        [idxEos, ~] = findchangepts( ...
            rssq(U_a_o.gettsafterevent(eventImpact).Data, 2), 'Statistic', 'rms', 'MaxNumChanges', 1);
        eventEos = tsdata.event('End of oscillation', U_a_o.gettsafterevent(eventImpact).Time(idxEos));
        
        annotateEvent(ax5(1), eventEos, 'bullet'); % Show in acc plot

        
        % Compute velocity in IMU frame from acceleration between impact
        % and .... Substract acceleration offset first.
        accOffset = mean(U_a_o_free.gettsbeforeatevent(eventStandstill).Data, 1);
        
        U_v_o = timeseries(cumtrapz( ...
            U_a_o_free.gettsafteratevent(eventImpact).Time, ...
            U_a_o_free.gettsafteratevent(eventImpact).Data), ...
            U_a_o_free.gettsafteratevent(eventImpact).Time);
        U_v_o.Name = 'Velocity at IMU origin in IMU';
        U_v_o.DataInfo.Units = 'm/sec';
        
        % Add integration constant / initial condition = velocity at impact
        U_v_o.Data = U_v_o.Data + ...
            (quat2rotm(quatinv(Q_WU.resample(eventImpact.Time).Data)) * ...
                W_v_oU.gettsatevent(eventImpact).Data')';
        
            
        % Compute velocity at F frame origin expressed in F frmae using
        % velocity transfer formula: F_v_o = F_v_oU + F_Omega * F_r_oUoF
        A_WU = quat2rotm(Q_WU.resample(U_v_o.Time).Data);
        [~, ~, A_WU_grad] = gradient(A_WU, 1, 1, U_v_o.Time);
                
        F_v_o = timeseries;
        F_v_o.Name = 'Velocity at F origin in F';
        F_v_o.DataInfo.Units = 'm/sec';
        
        for j = 1:U_v_o.Length
            A_FW = A_FC * A_CU * transpose(A_WU(:,:,j));
            F_v_oU = A_FC * A_CU * U_v_o.Data(j,:)';
            F_Omega = A_FW * A_WU_grad(:,:,j) * A_WU(:,:,j)' * A_FW';
            F_r_oUoF = A_FC * (( A_CU * U_ov_C' ) + C_ov_F' );
            
            % velocity transfer formula: Using matrix multiplication
            % instead of the cross product.
            F_v_o = addsample(F_v_o, ...
                'Data', ( F_v_oU + F_Omega * F_r_oUoF )', ...
                'Time', U_v_o.Time(j));
        end
        
        % Detect end of motion event. This is the point in time where velocity in direction
        % alongside the spear, i.e. in z axis, is minimal, or, if happening
        % before, where velocity first crosses zero.
        [~, idxEomMin] = min(abs(F_v_o.gettsbetweenevents(eventImpact, eventEos).Data(:,3)));
        
        % Inlinse function for identification of zero crossings
        zcr = @(v) find(v(:).*circshift(v(:), [-1 0]) <= 0); 
        
        idxEomCross = zcr(F_v_o.gettsbetweenevents(eventImpact, eventEos).Data(:,3));
        
        eventEom = tsdata.event('End of motion', F_v_o.gettsbetweenevents(eventImpact, eventEos).Time(min([idxEomMin; idxEomCross])));
        
        
        % Plot velocities in IMU frame and F frame together
        plotTimeseriesSync([U_v_o, F_v_o], 'parent', fig6); 
        ax6 = findobj(fig6, 'type', 'axes'); 
        grid(ax6(1), 'on');
        grid(ax6(2), 'on');
        annotateEvent(ax6(1), eventImpact, 'line');
        annotateEvent(ax6(2), eventEos, 'line');
        

        % Plot force and velocity together
        plotTimeseriesSync([F_v_o, F_F_o.gettsafterevent(eventTurning)], 'parent', fig7); 
        ax7 = findobj(fig7, 'type', 'axes');
        grid(ax7(2), 'on');
        annotateEvent(ax7(1), eventImpact, 'line');
        annotateEvent(ax7(1), eventEom, 'bullet');
        annotateEvent(ax7(2), eventEos, 'line');
        annotateEvent(ax7(2), eventEom, 'line');
        title(ax7(1), 'Force at F origin in F');
        set(ax7(1), 'xlim', [eventImpact.Time-0.2 eventEos.Time+0.2]);
        set(ax7(2), 'xlim', [eventImpact.Time-0.2 eventEos.Time+0.2]);
        
        % Error of the velocity at end of motion relative to impact event
        KinTable{i, 'WorkRelError'} = ...
            abs(mean(F_v_o.gettsbetweenevents(eventEom, eventEos).Data(:,3)) / F_v_o.Data(1,3));
        
        KinTable{i, 'EndOfMotion'} = eventEom;
        
        % Work applied to target/ energy transferred into target:
        % W = integral F_F_o * F_v_o dt
        % between impact and end of motion
        % units are in N*m/sec*sec = Nm = J
        % Consider only the z component. x and y are too much biased due to
        % acceleration offset. This assumtion is justified considering
        % directionality of the spear during thrusting.
        
        % Sync both timeseries
        vel =  F_v_o.gettsbetweenevents(eventImpact, eventEom);
        force = F_F_o.resample(vel.Time);
        
        % Compute work; using only z components 
        KinTable{i, 'WorkOnTarget'} = ...
            trapz(force.Time, dot( force.Data(:, 3), vel.Data(:, 3), 2));
        
        
    catch ME
        report.append(Text(sprintf('Energy computation failed: %s', ME.message)));
%         keyboard
    end

    
    
    %% ANGLES 
    % Introduce new frames to improve readability. Angles use
    % the RPY convention, similar to aircrafts. See documentation of
    % frames for details. Rotation in math. positive direction:
    % around z: roll
    % around y: pitch
    % around x: yaw    
    
    % Compute transform from Spear frame to tip frame in RPY angles
    RPY_TS = timeseries;
    
    if Trajectories{i, 'Modus'} == 'horizontal'
        RPY_TS.Name = 'RPY angles between horizontal reference and tip frame, rpy_{TSh}';
        A_ref2world = transpose(A_ShW);
    else % vertical
        RPY_TS.Name = 'RPY angles between vertical reference and tip frame, rpy_{TSv}';
        A_ref2world = transpose(A_SvW);
    end
            
    for j=1:Q_WU.Length
        RPY_TS = addsample(RPY_TS, ...
            'Data', rotm2eul( A_TU * quat2rotm(quatinv(Q_WU.Data(j,:))) * A_ref2world ), ...
            'Time', Q_WU.Time(j));
    end 
    RPY_TS.DataInfo.Units = 'deg';
    RPY_TS.Data = rad2deg(unwrap( RPY_TS.Data ));  % account for 2pi phase jumps 
    
    
    % Show angles
    plot(RPY_TS, 'parent', ax1);
    legend(ax1, 'roll', 'pitch', 'yaw');
    grid(ax1, 'on');
    annotateEvent(ax1, eventImpact, 'line');
    
    % Save angles at impact
    KinTable{i, 'ImpactAngles'} = RPY_TS.resample(eventImpact.Time).Data;

    % Angles defined by Eduard
    KinTable{i, 'Inclination'} = -KinTable.ImpactAngles(i,2);
    KinTable{i, 'Declination'} = -KinTable.ImpactAngles(i,3);
    
    
    %% KINETIC ENERGY BEFORE IMPACT
    
    % Calculate kin. energy: E = 1/2 m * v^2
    m_spear = 3.13; % mass of the spear in kg
    KinTable{i, 'KineticEnergy'} = 0.5 * m_spear * KinTable.SpeedAtImpactPos(i)^2;
    
    
    %% VISUALIZE in RVIZ
    
    if false
    tfmsg = appendTransform(tfmsg, 'U', 'C', U_ov_C, rotm2quat(inv(A_CU))); 
    tfmsg = appendTransform(tfmsg, 'U', 'L', U_ov_L, rotm2quat(A_UL)); 
    tfmsg = appendTransform(tfmsg, 'L', 'T', L_ov_T, rotm2quat(inv(A_TL))); 
    tfmsg = appendTransform(tfmsg, 'L', 'F', L_ov_F, rotm2quat(inv(A_FL))); 
    tfmsg = appendTransform(tfmsg, 'world', 'Sh', [0 0 0], rotm2quat(inv(A_ShW)));
    tfmsg = appendTransform(tfmsg, 'world', 'Sv', [0 0 0], rotm2quat(inv(A_SvW)));
    
    keyboard
    
    playTFSeries(tfmsg, 0.5);
    
    end
    
    
    
    %% FILL REPORT
    % Write new section for the next trial
    report.append(Heading(2, sprintf('%s, i = %u', Trajectories{i, 'ExpName'}, i)));
    report.append(mlReportTable(Trajectories(i, setdiff(Trajectories.Properties.VariableNames, {'Q_UC', 'U_ov_C'}))));
    
    % Add some individual comments
    switch Trajectories.ExpName(i)
        case {'BP102_1', 'BS117_1'}
            report.append(Text('This trial has a duplicate. One is invalid due to missing data'));
        case 'BP108_1'
            report.append(Text('This trial has invalid pose data due to tag occlusion during the experiment'));
    end
    
    report.append(Heading(3, 'Unprocessed Data'));
    report.append(addFigures(figHead1));
    
    if true
    report.append(Heading(3, sprintf('Aligned IMU and Pose Time Series')));
    report.append(addFigures(fig5));
    report.append(Text(sprintf('Free acceleration offset is %s m/sec^2', mat2str(accOffset, 3))));
    end
    
    if true
    report.append(Heading(3, sprintf('Computing velocities from turning to Impact')));
    report.append(addFigures(fig2));

    title(ax4, 'Velocities in world frame');
    xlabel(ax4, 'Time [sec]');
    ylabel(ax4, 'm/sec');
    legend(ax4, {'vAcc_x' 'vAcc_y', 'vAcc_z', 'Speed from acc', 'vPos_x', 'vPos_y', 'vPos_z'}, 'location', 'southwest');
    
    report.append(Text(sprintf('Speed at impact from acc data: %4.2f m/sec', KinTable{i, 'SpeedAtImpact'})));
    report.append(Text(sprintf('Speed at impact from pos data: %4.2f m/sec', KinTable{i, 'SpeedAtImpactPos'})));

    report.append(addFigures(fig4));
    report.append(Text('Acc: Computed by integrating IMU acceleration'));
    report.append(Text('Pos: Computed by deriving rcars pose estimate'));
    report.append(Text('Steps in vPos are caused by linear resampling of the raw pose data'));
    if Trajectories{i, 'Modus'} == 'horizontal'
        report.append(Text('Vel from acc invalid due to offset in acc data'));
    end
    end

    if true
    report.append(Heading(3, sprintf('Velocities in IMU and F frame')));
    report.append(Text('Velocities are computed by integration of acceleration data starting from impact event'));
    report.append(addFigures(fig6));
    report.append(Text('Offset in z component might be caused by saturation of the imu beyond 200 m/sec^2.'));
    end
    
    report.append(Heading(3, sprintf('Aligned force and velocity trajectories')));
    report.append(addFigures(fig7));
    
    report.append(Text(sprintf('Work applied to target: %4.2f J', KinTable{i, 'WorkOnTarget'})));
    report.append(Text(sprintf('Work rel error: %4.2f', KinTable{i, 'WorkRelError'})));
    report.append(Text(sprintf('Work rel error is due to IMU saturation induced error in velocity after impact')));
    report.append(Text(sprintf('Work is computed by integrating the scalar product of force and velocity from impact to end of motion')));
    
    if true
    report.append(Heading(3, sprintf('Orientation of the spear')));
    report.append(addFigures(fig1));
    end
    
    % Clear figur content
    clf(fig1);
    clf(fig2);
    clf(fig3);
    clf(fig4);
    clf(fig5);
    clf(fig6);
    clf(fig7);
    clf(figHead1);
    clf(figHead2);
    clf(figHead3);

end



% Finish and view
close(report);
close([fig1 fig2 fig3 fig4 fig5 fig6 fig7 figHead1 figHead2 figHead3]);

rptview(report.OutputPath);


%% SAVE RESULTS 

% Save to xls
targetFile = 'plot/out/Results_tmp.xls';
delete(targetFile);

% Select variables, sort after ImpactID, and write to xls file
writetable(sortrows(KinTable(:, ...
    {'ImpactID', 'ExpName', 'SpeedAtImpactPos', 'KineticEnergy', 'WorkOnTarget', 'WorkRelError'}), 1), ...
    targetFile);

% Append to mat file
%...

