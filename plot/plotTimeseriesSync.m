function fig = plotTimeseriesSync(series, varargin)
%plotTimeseriesSync Plot multiple timeseries in one plot using subplots.
%   Aligns time axis to be synchronous

validateattributes(series, {'timeseries'}, {'vector'}, mfilename, 'timeseries');

numTs = length(series);

dateTimeVecs = cell(1, numTs);



% Determine datetime vectors for each timeseseries
for i=1:numTs
    
    startDate = series(i).TimeInfo.StartDate;
    N = series(i).Length;
    
    if isempty(startDate)
        %error('All timeseries need to have a start date');
        startDate = 0;  
        minDate = [];
        maxDate = [];
    else
        minDate = datetime.empty;
        maxDate = datetime.empty;
    end
    
    if strcmpi(series(i).TimeInfo.Units, 'seconds')
        dateTimeVecs{i} = startDate + seconds(series(i).Time);
    else
        error('Time unit must be in seconds');
    end
        
    minDate = min([dateTimeVecs{i}; minDate]);
    maxDate = max([dateTimeVecs{i}; maxDate]);
end

% Check input arguments for parent figure
if nargin == 3 && strcmp(varargin{1}, 'parent')
    fig = varargin{2};
else
    fig = figure;
end

hx = [];

for i=1:numTs
    
    hx(i) = subplot(numTs, 1, i, 'parent', fig);
    
    plot(series(i), 'parent', findobj(hx(i), 'Type', 'axes'));
    
    if isdatetime(startDate)
        xtickformat(hx(i), 'ss.SSS');
    end
    
    ylabel(hx(i), sprintf('[%s]', series(i).DataInfo.Units));
    title(hx(i), series(i).Name); 
    
    legend('show');
    
end

if isdatetime(startDate)
    xlim = [minDate maxDate];
else
    xlim = seconds([minDate maxDate]);
end

set(hx, 'Xlim', xlim);

xlabel(hx(end), 'Time [sec]');


end