% Figure for the SI article: Impact detection in timeseries of IMU data
% Figure Name: Supplementary Figure x6

DATA = load('data/dataset05.mat');

data = outerjoin(DATA.TRAJ, DATA.KinTable, 'keys', 'ExpName', ...
    'RightVariables', ...
    {'EndOfMotion'});

trialNo = 2;

name = data.ExpName(trialNo);
lac = data.U_a_o(trialNo);

eventImpact = data.Impact(trialNo);
eventEOM = data.EndOfMotion(trialNo);

threshold = 3e+3;

lacGrad = gradient(rssq(lac.Data, 2), lac.Time);


%% Plot

figure;
set(gcf, 'position', [800 300, 900, 400]);

% Plot linear acceleration
plot(lac, '-');

annotateEvent(gca, eventImpact, 'line');
annotateEvent(gca, eventEOM, 'line');

hold on;

hold off;
ax = gca;
set(gca, 'xlim', [2.7 3.3]);
title('');
ax.XLabel.String = 'Time [sec]';
ax.YLabel.String = 'Acceleration [m/sec²]';

grid on;

textLabels = findobj(gca, 'Type', 'Text');
delete(textLabels);

text(ax, 3.07, 130, 'end');
text(ax, 2.99, 130, 'start', 'HorizontalAlignment', 'right');

% saveas(gcf, 'plot/out/SupplementaryFigureX6.eps', 'eps');

% Write Captions as txt file
fprintf(fopen('plot/out/SupplementaryFigureX6.txt', 'w+'), ...
    ['Timeseries of the linear acceleration with impact event of %s. ' ...
    'Start and end of the impact phase are indicated by the red vertical lines.'], ...
    data.ExpName(1));




