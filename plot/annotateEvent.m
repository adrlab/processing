function [ ] = annotateEvent( myax, event, style )
%annotateEvent Annotate a tsdata.event object in a time series plot.
%  Annotation of the event is written into axes, myax using different
%  style elements to represent the time: Vertical line (line), bullet, etc.
%  Style element is specified by string, style.

validateattributes(myax, {'matlab.graphics.axis.Axes'}, {'scalar'}, mfilename, 'axes');
validateattributes(event, {'tsdata.event'}, {'scalar'}, mfilename, 'event');
validateattributes(style, {'char'}, {'row'}, mfilename, 'style specifier');

hold(myax, 'on');
    
% Check time specifications
if isdatetime(event.EventData)
    if strcmpi(event.Units, 'seconds')
        timestamp = event.EventData + seconds(event.Time);
    else
        error('Event time must be seconds');
    end
    timestamp.TimeZone = '';
else
    timestamp = event.Time;
end

switch lower(style)
    case 'line'
        % Draw vertical line from minimum to maximum y axis value at the
        % timestamp of the event.
        ylim = get(myax, 'Ylim');

        p1 = line(myax, repmat(timestamp, 1, 2), ylim, ...
            'Color', 'r', 'Linewidth', 0.3);

        % Tag it, so it can be found
        p1.Tag = sprintf('VertLine %s', event.Name); 

        % place text at 5 % below upper limit
        text(timestamp, ylim(2)-abs(diff(ylim))*0.05, event.Name, ...
            'HorizontalAlignment', 'center', 'Parent', myax);
        
    case 'bullet'
        % Draw a dot onto the line at timestamp of the event
        
        hlines = findobj(myax,'Type','line');
        
        % Draw on each line
        for i=1:length(hlines)            
            % Draw a point at the interpolated position
            % Only on data with more than 2 elements
            if length(unique(hlines(i).XData)) > 1 
                y = interp1(hlines(i).XData, hlines(i).YData, timestamp);
            
                plot(timestamp, y, ...
                    'o', 'MarkerSize', 5, 'MarkerFaceColor', 'm', 'Parent', myax);
                
                text(timestamp, double(y), event.Name, ...
                    'HorizontalAlignment', 'left', 'Parent', myax);
            end
        end
        
        
    otherwise
        error('Style element is not supported');
end
        
end



