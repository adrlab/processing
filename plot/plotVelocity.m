%plotVelocity Compare velocities and kin. E. between different groups.
%   The data is first fused by joining related tables using keys variabels.
%   Groups are built based on table variable values and plotted.


% Load dataset:
DATA = load('data/dataset05.mat');

% Relate trajectory data with pelvis table and keep only relevant variabels
data = innerjoin(DATA.KinTable, DATA.PARA, 'keys', 'ExpName', ...
    'LeftVariables', ...
    {'ExpName', 'VelocityAtImpact', 'VelocityAtImpactPos', 'SpeedAtImpactPos', 'WorkOnTarget', 'ImpactAngles', 'PeakForceZ'}, ...
    'RightVariables', ...
    {'ImpactID', 'Target', 'Impactonbone', 'Damageonbone', 'Modus'});


%% Compute kinetic energies for all trials
speed = data.SpeedAtImpactPos;

% Calculate kin. energy: E = 1/2 m * v^2
m_spear = 3.13; % mass of the spear in kg
kinEnergy = 0.5*m_spear*(speed.*speed);

results = [ data, ...
    table(kinEnergy, 'VariableNames', {'KineticEnergy'}), ...
    table(-data.ImpactAngles(:,2), -data.ImpactAngles(:,3), 'VariableNames', {'Inclination', 'Declination'}) ];

% Sort after ImpactID
results = sortrows(results, 1);

targetFile = 'plot/out/Results_all.xls'; delete(targetFile);
writetable(results, targetFile);

targetFile = 'plot/out/Results_peakForce.xls'; delete(targetFile);
writetable(results(:, {'ImpactID', 'ExpName', 'PeakForceZ'}), targetFile);

targetFile = 'plot/out/Results_angles.xls'; delete(targetFile);
writetable(results(:, {'ImpactID', 'ExpName', 'Inclination', 'Declination', 'Modus'}), targetFile);

targetFile = 'plot/out/Results_energy.xls'; delete(targetFile);
writetable(results(:, {'ImpactID', 'ExpName', 'WorkOnTarget', 'KineticEnergy', 'Modus'}), targetFile);

 
%% Synbone Fracture Velocity - 3 class typology
% Specify three classes and get corresponding data CHECK IF NUMBERS CORRECT
% ClassA: more than 4 fractures
classA = [66,49,50,56,57,69,74,79,53,83,54,65,67,70,82,76]; 
% ClassB: 4 fractures
classB = [42,51,68,78,45,48,55,64,44,43,63,47];             
% ClassC: leq than 3 fractures
classC = [58,60,59,73,80,61,62,52,46];                      

vA = TRAJ.VelocitiesAtImpact(classA, 3);
vB = TRAJ.VelocitiesAtImpact(classB, 3);
vC = TRAJ.VelocitiesAtImpact(classC, 3);

figure;
scatter([ones(size(vA)); 2*ones(size(vB)); 3*ones(size(vC))], [vA; vB; vC], 'kx');
title('Synbone Fracture Velocities: Group A vs. Group B vs. Group C');
xlabel('Fracture Pattern');
ylabel('Velocity [m\sec]');
axis([0, 4, 0, 7]);
set(gca,'YMinorGrid','on');

% formatPlot('Synbone Fracture Velocity Comparison: Group A vs. Group B vs. Group C',...
%    'Fracture Pattern','Velocity [$\frac{m}{s}$]',{'A','B','C'});
% print(['vel_syn_AvsBvsC'],'-dpdf','-bestfit');
% print(['vel_syn_AvsBvsC'],'-djpeg');



%% Trials with pelvis elaphus (Becken)
isPelvis = data.Target == 'Bone Pelvis (BP)';

% speedPelvis = rssq(data.VelocitiesAtImpact(isPelvis, :), 2);
speedPelvis = results{isPelvis, 'SpeedAtImpactPos'};

figure;
set(gcf, 'Position', get(gcf, 'Position')-[0 0 0 0]);
scatter(ones(size(speedPelvis)), speedPelvis, 'kx');
title('Velocity before impact of pelvis elaphus');
ylabel('Velocity [m/sec]');
set(gca, 'YMinorGrid', 'on');
set(gca, 'XTick', []);

% Move axis to the side so we have space for some text
set(gca, 'Position', [0.1 0.1 0.6 0.8]);

% Create new axis for text
atx = axes;
set(atx, 'Parent', gcf, 'Units', 'normalized', ...
    'Position', [0.75 0 0.2 1], 'Visible', 'off');

% Write some details into axes
text(0.1, 0.9, sprintf('Mean: %3.2f', mean(speedPelvis, 'omitnan')), ...
    'Parent', atx, 'Units', 'normalized');
text(0.1, 0.8, sprintf('Variance: %3.2f', var(speedPelvis, 'omitnan')), ...
    'Parent', atx, 'Units', 'normalized');
    
    

%% Impact labels: select data
% Analyse performance of the thrust from Ellis labels in
% NNRepExpFracturesBP.
% Define two groups (green, yellow) within the impact labels
isSlippOff = data.Impactdescription == 'slipped off' | ...
            data.Impactdescription == 'slipped off or not full impact';

isFullHit = data.Impactdescription == 'full impact on cranial part';

vars = {'HitID', 'VelocitiesAtImpact', 'C_pos', 'ExpName'};

datFull=data(isFullHit, vars);
datSlip=data(isSlippOff, vars);


%% Impact lables: velocities at impact

figure;
set(gcf, 'Position', get(gcf, 'Position')-[0 0 0 0]);

% CAN USE datFull/ datSlip HERE:
scatter( ...
    [ones(sum(isSlippOff),1); 2*ones(sum(isFullHit),1)], ...
    [rssq(data.VelocitiesAtImpact(isSlippOff, :),2); rssq(data.VelocitiesAtImpact(isFullHit, :),2)], ...
    'kx'); 

title('Velocity before impact of pelvis elaphus');
ylabel('Velocity [m/sec]');
set(gca, 'YMinorGrid', 'on');
set(gca, 'XLim', [0 3]);
set(gca, 'XTick', [1 2]);
set(gca, 'XTickLabel', { ...
    sprintf( ...
    '\\begin{tabular}{c} slipp off or lax hit (y) \\\\ n=%u\\end{tabular}', ...
    sum(isSlippOff)), ...
    sprintf( ...
    '\\begin{tabular}{c} full hit (g) \\\\ n=%u\\end{tabular}', ...
    sum(isFullHit))});
set(gca, 'TickLabelInterpreter', 'latex');

% Add color label and export data to file
datFull.Color=repmat({'g'}, sum(isFullHit),1);
datSlip.Color=repmat({'y'}, sum(isSlippOff),1);

out=[datFull; datSlip];
out.VelocityAtImpactABS = rssq(out.VelocitiesAtImpact, 2);

writetable(out, 'plot/out/velPelvisImpacts.xls');



