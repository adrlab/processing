%preprocess Do preprocessing of raw trajectories in datasetRAW.mat
% - Check time series by visual inspection
% - Identify impact, for the timeseries of each sensor separately
% - Resample all timesereis on a common time vector which is cropped aroung
%   the impact
% - Identify more events in the timeseries

clear all; close all;

% Load table of raw data
load('data/datasetRAW.mat');

% Join tables including modus of the trial(horizontal/ vertical)
PARA.ExpName = cellstr(char(PARA.ExpName));

data = innerjoin( ...
    PARA, outerjoin( ...
        BagTable, LogTable, 'keys', {'ExpName'}), ...
    'leftkeys', {'ExpName'}, 'rightkeys', {'ExpName_BagTable'}, ...
    'leftvariables', {'Modus'});

% Deselect invalid rows
data = data(~logical(sum(ismissing(data(:, {'ExpName_LogTable'})), 2)), :);

% Deselect
data = data(:, :);

timeZoneOfExperiment = 'Europe/Zurich';


%% Initialize
% Store processed trajectories in TRAJ, Initialize
TRAJ = table();
TRAJ.ExpName = string(data.ExpName_LogTable);
TRAJ{:, 2} = NaT;
TRAJ{:, 3:7} = timeseries;
TRAJ{:, 8} = tsdata.event;
TRAJ{:, 9} = tsdata.event;

% Add Description; adopt from table of raw data
TRAJ.Properties.Description = 'Processed trajectories with uniform time vector';
TRAJ.Properties.VariableNames = ...
    {'ExpName', 'StartDate', 'Q_WU', 'W_ov_U', 'U_a_o', 'F_F_o', 'F_T_o', 'Impact', 'Standstill'};
TRAJ.Properties.VariableDescriptions = ...
    [{'', sprintf('Start date and time of the timeseries in %s', timeZoneOfExperiment)}, ...
    BagTable.Properties.VariableDescriptions(4:5), ...
    LogTable.Properties.VariableDescriptions(2), ...
    LogTable.Properties.VariableDescriptions(4:5), ...
    {'Impact event detected from IMU trajectory', ...
    'Standstill event detected from pose trajectory'}];

TRAJ = [TRAJ data(:, 8:9)]; % Add constant COS trafos


%% Start Loop
figAll = zeros(height(data), 2);

for i_exp = 1:height(data)
       
    fprintf('Process experiment %s\n', TRAJ{i_exp, 'ExpName'}{:});
    
    pos = data{i_exp, 'W_ov_U'};
    ori = data{i_exp, 'Q_UW'};
    
    lac = data{i_exp, 'U_a_o'};
    ave = data{i_exp, 'U_w_UW'};
    
    force = data{i_exp, 'F_F_o'};
    torque = data{i_exp, 'F_T_o'};
    
    pos.TimeInfo.StartDate = data{i_exp, 'StartDate'};
    ori.TimeInfo.StartDate = data{i_exp, 'StartDate'};
    
    % Take out time zone. Otherwise comparison is not possible
    pos.TimeInfo.StartDate.TimeZone = '';
    ori.TimeInfo.StartDate.TimeZone = '';
    lac.TimeInfo.StartDate.TimeZone = '';
    force.TimeInfo.StartDate.TimeZone = '';
    torque.TimeInfo.startDate.TimeZone = '';
    
    % PLOT ALL
    if false
        figAll(i_exp, 1) = plotTimeseriesSync([pos, lac, force]);
        set(figAll(i_exp, 1), 'Name', TRAJ{i_exp, 'ExpName'}{:});
    end
    
    % DETECT IMPACT
    % Detect impact in each sensor clock and shift time vector to be 
    % in sync with time vector of the IMU clock.
    
    % IMU clock
    % Compute derivative of norm of acceleration and detect threshold crossing
    threshold = 3e+3;
    idxImpactIMU = find( ...
        gradient(rssq(lac.Data, 2), lac.Time) > threshold, 1, 'first');

    if isempty(idxImpactIMU)
        continue
    end
    
    timeImpactIMU = lac.Time(idxImpactIMU);
    
    % F/T clock
    threshold = 0.5e+4;
    timeImpactFT = force.Time(find( ...
        gradient(rssq(force.Data, 2), force.Time) > threshold, 1, 'first'));
    
    force.Time = force.Time - (timeImpactFT - timeImpactIMU); 
    torque.Time = torque.Time - (timeImpactFT - timeImpactIMU);
        
    % Pose clock    
    if data{i_exp, 'Modus'} == 'horizontal'
        [~, idxImpactPose] =  min(gradient(pos.Data(:,1)));
    elseif data{i_exp, 'Modus'} == 'vertikal'
        [~, idxImpactPose] =  min(gradient(pos.Data(:,3)));
    else
        error('Undefined modus');
    end
    
    timeImpactPose = pos.Time(idxImpactPose);
        
    pos.Time = pos.Time - (timeImpactPose - timeImpactIMU);
    ori.Time = ori.Time - (timeImpactPose - timeImpactIMU);
    

    isRef = lac.Time > timeImpactIMU - 3 & lac.Time < timeImpactIMU + 2 ;
    
    deltaT = mean(diff(lac.Time));
    startRef = lac.Time(find(isRef, 1, 'first'));
    startRefGlob = lac.TimeInfo.StartDate + seconds(startRef);
    timeRef = startRef + (0:1:sum(isRef))*deltaT;
    
    % TO DO write function for this
    lac.TimeInfo.StartDate = [];
    lac = lac.resample(timeRef, 'linear');
    lac.Time = timeRef - startRef;

    ave.TimeInfo.StartDate = [];
    ave = ave.resample(timeRef, 'linear');
    ave.Time = timeRef - startRef;
    
    force.TimeInfo.StartDate = [];
    force = force.resample(timeRef, 'linear');
    force.Time = timeRef - startRef;
    
    torque.TimeInfo.StartDate = [];
    torque = torque.resample(timeRef, 'linear');
    torque.Time = timeRef - startRef;
    
    pos.TimeInfo.StartDate = [];
    pos = pos.resample(timeRef, 'linear');
    pos.Time = timeRef - startRef;
    
    ori.TimeInfo.StartDate = [];
    ori = ori.resample(timeRef, 'linear');
    ori.Time = timeRef - startRef;
    
    if true
        figAll(i_exp, 2) = plotTimeseriesSync([lac, force, pos]);
    end
    
        
    % DEFINE EVENTS
    % Impact as the sample beeing closest to the time determined above.
    [~, idxImpact] = min(abs(timeRef - timeImpactIMU));
    eventImpact = tsdata.event('Impact', lac.Time(idxImpact));
    
    % Standstill
    % As the timestamp when velocity from pose before the impact is lowest
    [~, idxStandstill] = min(rssq([ ...
        gradient(pos.gettsbeforeevent(eventImpact).Data(:,1), timeRef), ...
        gradient(pos.gettsbeforeevent(eventImpact).Data(:,2), timeRef), ...
        gradient(pos.gettsbeforeevent(eventImpact).Data(:,3), timeRef)], 2));
    eventStandstill = tsdata.event('Standstill', timeRef(idxStandstill) - startRef);

    % Mark events in plots
    if true
        myaxes = findobj(figAll(i_exp, 2), 'type', 'axes');
        annotateEvent(myaxes(1), eventImpact, 'bullet');
        annotateEvent(myaxes(2), eventImpact, 'line');
        annotateEvent(myaxes(2), eventStandstill, 'line');
        annotateEvent(myaxes(3), eventImpact, 'line');
    end
    
    % STORE IN TRAJ
    TRAJ{i_exp, 'StartDate'} = startRefGlob;
    
    % Save trajectories
    TRAJ{i_exp, 'Q_WU'}     = ori;
    TRAJ{i_exp, 'W_ov_U'}   = pos;
    
    TRAJ{i_exp, 'U_a_o'}    = lac;
    
    TRAJ{i_exp, 'F_F_o'}    = force;
    TRAJ{i_exp, 'F_T_o'}    = torque;
    
    % Save events
    TRAJ{i_exp, 'Impact'}   = eventImpact;
    TRAJ{i_exp, 'Standstill'} = eventStandstill;

end


% % Check that const trafos are constant over all trials
if any(var(TRAJ{:, 'Q_UC'}, 1) > 1e-10) || any(var(TRAJ{:, 'U_ov_C'}, 1) > 1e-10)
    error('Camera to IMU transform is not constant');
end



%% Save to mat file

targetFile = 'data/dataset05.mat';

if exist(targetFile, 'file') == 0     % file does not exist jet
    save(targetFile, 'TRAJ');
else
    answer = questdlg( ...
        sprintf('File %s already exists. Do you want to overwrite TRAJ?', targetFile), ...
        'Overwrite file?');
    
    if strcmpi(answer, 'yes')
        save(targetFile, '-append', 'TRAJ');
    else
        % Do not save, just quit.
    end
end






