%importBagfiles Read out bagfiles and preprocess for storage as timeseries.

clear all; 

dirBagfiles = '../Bag_Files';

filelist = dir(fullfile(dirBagfiles, '*.bag'));

filelist = filelist(1:end); % select subset of bagfiles

% Store timeseries objects in here
BagTable = table();    
BagTable.Properties.Description = ...
    'Table of /tf messages read from bag files and processed transforms';


%% Loop over bag files
for i_bag = 1:length(filelist)
    
myfile = fullfile(dirBagfiles, filelist(i_bag).name);

% Extract Experiment Name from filename
expName = regexp(filelist(i_bag).name, '^(\w+\d+_\d)_', 'tokens');

% Read /tf messages out of bag-file
bag = rosbag(myfile);

topic_tf = select(bag, 'Topic', '/tf');

if topic_tf.NumMessages < 1
    BagTable{i_bag, 'ExpName'} = {''};
    continue;
end

tfMsgs = readMessages(topic_tf);

% Expand all messages
tfMsgs = [tfMsgs{:}];
tfMsgsTrafo = [tfMsgs.Transforms];
tfMsgsTrafoHeader = [tfMsgsTrafo.Header];
tfMsgsTrafoHeaderStamp = [tfMsgsTrafoHeader.Stamp];

% Get common start time in posix seconds
startDate = datetime(bag.StartTime, 'ConvertFrom', 'posixtime', 'timezone', 'Europe/Zurich');
endDate = datetime(bag.EndTime, 'ConvertFrom', 'posixtime', 'timezone', 'Europe/Zurich');
startTimeSec = bag.StartTime;

clear bag;

%% Group transformations
BagTable{i_bag, 'ExpName'}      = expName{:};
BagTable{i_bag, 'StartDate'}    = startDate;
BagTable{i_bag, 'EndDate'}      = endDate;

if i_bag == 1
BagTable.Properties.VariableDescriptions(2) = {'Start date of the bag recording with 1 second accuracy'};
BagTable.Properties.VariableDescriptions(3) = {'End date of the bag recording'};
end

rosMsgTrafo = timeseries.empty;

% Find unique source frames
[sourceGroups, sourceNames] = findgroups({tfMsgsTrafoHeader.FrameId});

% loop through source frames
for i=1:length(sourceNames)
    
    % Select messages of this source frame
    idxSource = find(sourceGroups == find(ismember(sourceNames, sourceNames(i))));

    % Find unique target frames corresponding to this source frame
    [targetGroups, targetNames] = findgroups({tfMsgsTrafo(idxSource).ChildFrameId});
    
    % loop through target frames
    for j=1:length(targetNames)
        
        % Get target indices as subset of source indices
        idxTarget = find(targetGroups == find(ismember(targetNames, targetNames(j))));

        % Compute time vector in seconds starting from 0
        timeSec = [tfMsgsTrafoHeaderStamp(idxSource(idxTarget)).Sec];
        timeNsec = [tfMsgsTrafoHeaderStamp(idxSource(idxTarget)).Nsec];
        
        % Reset seconds using integer arithmetic. Thens add nano second in
        % floating point arithmetic
        timeVec = double(int32(timeSec) - int32(startTimeSec)) + timeNsec * 1e-9;

        if any(diff(timeVec) < 0)
            keyboard
        end
        
        % Store tranform between source and target as timeseries object
        rosMsgTrafo = timeseries( ...
            [tfMsgsTrafo(idxSource(idxTarget)).Transform], timeVec, ...
            'Name', sprintf('%s_to_%s', sourceNames{i}, targetNames{j}));

        rosMsgTrafo.TimeInfo.Units = 'Seconds';
        rosMsgTrafo.TimeInfo.UserData.TimeSec = timeSec;
        rosMsgTrafo.TimeInfo.UserData.TimeNsec = timeNsec;
        
        if strcmp(rosMsgTrafo.Name, 'rcars_camera_to_rcars_tag4_detector')
%             keyboard
        end
        
        % The transformations are defined as:
        % object in target frame (trafo)-> object in source frame
        % E.g. HomTrans_ST has S: source, T: target
        switch rosMsgTrafo.Name
            case 'rcars_workspace_to_reference_tag'
                HomTrans_WR = rosMsgTrafo;
                
            case 'rcars_workspace_to_rcars_inertial'
                HomTrans_WI = rosMsgTrafo;
                
            case 'rcars_inertial_to_rcars_IMU'
                HomTrans_IU = rosMsgTrafo;
                
            case 'rcars_IMU_to_rcars_camera'
                HomTrans_UC = rosMsgTrafo;
        end
        
        
    end
end
    

%% Plot time vectors

if false % switch off
figure; hold on;
plot(HomTrans_WR.Time, 1, 'b*');
plot(HomTrans_WI.Time, 2, 'r*');
plot(HomTrans_IU.Time, 3, 'g*');
plot(HomTrans_UC.Time, 4, 'r*');
grid on;
end


%% Catenate transforms
% Compute the catenated transformations for:
% - world to IMU        UW
% - world to camera     CW
% - IMU to camera       CU
% world, rcars_workspace and rcars_reference should be the same.
% Frame tokens
% W: world 
% R: refrence tag 
% U: IMU 
% C: Camera
% I: rcars inertial frame

% Declare rotation and translation of origin as timeseries objects
Q_UC = [];
U_ov_C = [];

Q_WR = [];
W_ov_R = [];

Q_WU = timeseries();
Q_WU.Name = 'IMU To world Rotation';
Q_WU.DataInfo.Units = 'Unit Quaternion';

W_ov_U = timeseries();
W_ov_U.Name = 'Origin of IMU with respect to world';
W_ov_U.DataInfo.Units = 'm';

Q_WC = timeseries();
Q_WC.Name = 'Camera to world Rotation';
Q_WC.DataInfo.Units = 'Unit Quaternion';

W_ov_C = timeseries();
W_ov_C.Name = 'Origin of camera with respect to world';
W_ov_C.DataInfo.Units = 'm';


% Get timestamps common to all timeseries
timestamps = intersect(HomTrans_WR.Time, ...
    intersect(HomTrans_WI.Time, ...
    intersect(HomTrans_IU.Time, HomTrans_UC.Time)));

for i=1:length(timestamps)
    % Store data of one timestamp. Note definition of source and target rotations!
    q_WR = HomTrans_WR.getsampleusingtime(timestamps(i)).Data.Rotation;
    q_WI = HomTrans_WI.getsampleusingtime(timestamps(i)).Data.Rotation;
    q_IU = HomTrans_IU.getsampleusingtime(timestamps(i)).Data.Rotation;
    q_UC = HomTrans_UC.getsampleusingtime(timestamps(i)).Data.Rotation;
    
    w_ov_R = HomTrans_WR.getsampleusingtime(timestamps(i)).Data.Translation;
    w_ov_I = HomTrans_WI.getsampleusingtime(timestamps(i)).Data.Translation;
    i_ov_U = HomTrans_IU.getsampleusingtime(timestamps(i)).Data.Translation;
    u_ov_C = HomTrans_UC.getsampleusingtime(timestamps(i)).Data.Translation;
    
    % Apply rotational transformations using MATLABs quaternion definition
    % world to rcars inertial, then rcars inertial to IMU. 
    % (deprecated) Use: R_31 = inv(R_12*R_23) = inv(R_23) * inv(R_12) = R_32 * R_21
    Q_WU = Q_WU.addsample('Time', timestamps(i), ...
        'Data', quatmultiply([q_WI.W q_WI.X q_WI.Y q_WI.Z], [q_IU.W q_IU.X q_IU.Y q_IU.Z]));
    
    % world to IMU, then IMU to camera
    Q_WC = Q_WC.addsample('Time', timestamps(i), ...
        'Data', quatmultiply(quatinv(Q_WU.Data(end, :)), [q_UC.W q_UC.X q_UC.Y q_UC.Z]));
    
    Q_UC(i, :) = [q_UC.W q_UC.X q_UC.Y q_UC.Z];
    Q_WR(i, :) = [q_WR.W q_WR.X q_WR.Y q_WR.Z];
    
    % Apply translational trasnformations
    % Trnsform IMU origin vector from rcars inertial frame to world frame,
    % then add it to the vector of origin of rcars inertial frame
    W_ov_U = W_ov_U.addsample('Time', timestamps(i), 'Data', ...
        [w_ov_I.X w_ov_I.Y w_ov_I.Z] + ...
        (quat2rotm([q_WI.W q_WI.X q_WI.Y q_WI.Z]) * [i_ov_U.X; i_ov_U.Y; i_ov_U.Z])');
        
    % Transform origin of camera frame to world reference first, then add
    % to origin of IMU frame
    W_ov_C = W_ov_C.addsample('Time', timestamps(i), 'Data', ...
        W_ov_U.Data(end, :) + ...
        (quat2rotm(Q_WU.Data(end, :)) * [u_ov_C.X; u_ov_C.Y; u_ov_C.Z])' );

    
    % Those should be constant
    U_ov_C(i, :) = [u_ov_C.X u_ov_C.Y u_ov_C.Z];
    W_ov_R(i, :) = [w_ov_R.X w_ov_R.Y w_ov_R.Z];
    
end
    

fprintf('No: %u, %s\n', i_bag, expName{:}{:});

% Check IMU to camera transform
fprintf('Camera to IMU rotation has mean, and variance:\n')
disp(mean(Q_UC, 1));
disp(var(Q_UC, 1));

fprintf('IMU to camera translation has mean, and variance:\n');
disp(mean(U_ov_C, 1));
disp(var(U_ov_C, 1));

% Check vworld to reference frame 
fprintf('World to reference rotation has mean, and variance:\n')
disp(mean(Q_WR, 1));
disp(var(Q_WR, 1));

fprintf('World to reference translation has mean, and variance:\n');
disp(mean(W_ov_R, 1));
disp(var(W_ov_R, 1));




%%  Save to table
nVar = width(BagTable);
BagTable{i_bag, 'Q_UW'}     = Q_WU;
BagTable{i_bag, 'W_ov_U'}   = W_ov_U;
BagTable{i_bag, 'Q_CW'}     = Q_WC;
BagTable{i_bag, 'W_ov_C'}   = W_ov_C;
BagTable{i_bag, 'Q_UC'}     = mean(Q_UC);
BagTable{i_bag, 'U_ov_C'}   = mean(U_ov_C);
BagTable{i_bag, 'Q_WR'}     = mean(Q_WR);
BagTable{i_bag, 'W_ov_R'}   = mean(W_ov_R);

if i_bag==1
BagTable.Properties.VariableDescriptions(end-7) = {[Q_WU.Name ', quaternion as [w x y z]']};
BagTable.Properties.VariableDescriptions(end-6) = {[W_ov_U.Name ', vector as [x y z]']};
BagTable.Properties.VariableDescriptions(end-5) = {[Q_WC.Name ', quaternion as [w x y z]']};
BagTable.Properties.VariableDescriptions(end-4) = {[W_ov_C.Name ', vector as [x y z]']};
BagTable.Properties.VariableDescriptions(end-3) = {'Camera to IMU rotation, quaternion as [w x y z]'};
BagTable.Properties.VariableDescriptions(end-2) = {'Origin of camera with respect to IMU, vector as [x y z]'};
BagTable.Properties.VariableDescriptions(end-1) = {'Reference frame to world rotation, quaternion as [w x y z]'};
BagTable.Properties.VariableDescriptions(end)   = {'Origin of reference frame with respect to world, vector as [x y z]'};
end

end


%% Save to matfile

targetFile = 'data/datasetRAW.mat';

if exist(targetFile, 'file') == 0     % file does not exist jet
    save(targetFile, 'BagTable');
else
    answer = questdlg( ...
        sprintf('File %s already exists. Do you want to overwrite BagTable?', targetFile), ...
        'Overwrite file?');
    
    if strcmpi(answer, 'yes')
        save(targetFile, '-append', 'BagTable');
    else
        % Do not save, just quit.
    end
end






