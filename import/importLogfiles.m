%importLogfiles Read out logfiles of IMU and F/T measurements.

clear all

% Store timeseries objects in here
LogTable = table();    
LogTable.Properties.Description = ...
    'Table of sensor measurements read from log files';


dir1 = '../Dante/log';
dir2 = '../Diana/log';

format = '%u32%u32%f32%f32%f32%f32%f32%f32';

%% Read IMU files
token = '*imu.txt';

filelist1 = dir(fullfile(dir1, token));
filelist2 = dir(fullfile(dir2, token));
filelist = [filelist1; filelist2];
filelist = filelist(1:end); % select subset of bagfiles

for i_log = 1:length(filelist)
    expName = regexp(filelist(i_log).name, '^(\w+\d+_\d)_', 'tokens');

    LogTable{i_log, 'ExpName'} = expName{:};
    % ismember(LogTable.ExpName, expName);


    filename = fullfile(filelist(i_log).folder, filelist(i_log).name);

    source = fopen(filename,'r');
    if source == -1
        error('Could not open file %s', filename);
    end    
    data = textscan(source, format, 'Delimiter', ',', 'EmptyValue', NaN);
    fclose(source);
    
    % 1481622359,288614395,5.13257,-0.999739,-8.25877,0.00601709,-0.0146627,-0.0113845

    timeSec = data{1};
    timeNsec = data{2};

     startDate = datetime(timeSec(1), 'ConvertFrom', 'posixtime', 'timezone', 'Europe/Zurich');
%     LogTable{tRow, 'EndDate'} = datetime(timeSec(end), 'ConvertFrom', 'posixtime', 'timezone', 'Europe/Zurich');

    timestamps = double(timeSec-timeSec(1)) + double(timeNsec) * 1e-9;

    U_a_o = timeseries([data{3} data{4} data{5}], timestamps, 'Name', ...
        'Linear acceleration of IMU origin in IMU');
    U_a_o.DataInfo.Units = 'm/sec2';
    U_a_o.TimeInfo.StartDate = startDate;

    U_w_UW = timeseries([data{6} data{7} data{8}], timestamps, 'Name', ...
        'Angular velocity of world to IMU in IMU');
    U_w_UW.DataInfo.Units = 'rad/sec^2';
    U_w_UW.TimeInfo.StartDate = startDate;
    
    LogTable{i_log, 'U_a_o'} = U_a_o;
    LogTable{i_log, 'U_w_UW'} = U_w_UW;
end

% Set description
LogTable.Properties.VariableDescriptions = ...
    {'', ...
    'Linear acceleration of IMU origin expressed in IMU frame, [x y z]', ...
    'Angular velocity of world frame to IMU frame expressed in IMU frame, [x y z]'};


%% Read FT files
token = '*ft.txt';

filelist1 = dir(fullfile(dir1, token));
filelist2 = dir(fullfile(dir2, token));
filelist = [filelist1; filelist2];
filelist = filelist(1:end); % select subset of bagfiles

for i_log = 1:length(filelist)
    expName = regexp(filelist(i_log).name, '^(\w+\d+_\d)_', 'tokens');
    
    isExp = ismember(LogTable.ExpName, expName{:});

    if sum(isExp) == 0
        % add new row to table
        tRow = height(LogTable) + 1;
        LogTable{tRow, 'ExpName'} = expName{:};
    elseif sum(isExp) == 1
        tRow = find(isExp);
    else
        error('No row found');
    end
    
    filename = fullfile(filelist(i_log).folder, filelist(i_log).name);

    source = fopen(filename,'r');
    if source == -1
        error('Could not open file %s', filename);
    end    
    data = textscan(source, format, 'Delimiter', ',', 'EmptyValue', NaN);
    fclose(source);
    
    % 1481622359,288614395,5.13257,-0.999739,-8.25877,0.00601709,-0.0146627,-0.0113845

    timeSec = data{1};
    timeNsec = data{2};

    startDate = datetime(timeSec(1), 'ConvertFrom', 'posixtime', 'timezone', 'Europe/Zurich');
%     LogTable{tRow, 'EndDate'} = datetime(timeSec(end), 'ConvertFrom', 'posixtime', 'timezone', 'Europe/Zurich');

    timestamps = double(timeSec-timeSec(1)) + double(timeNsec) * 1e-9;

    F_F_o = timeseries([data{3} data{4} data{5}], timestamps, 'Name', ...
        'Force in F at origin ');
    F_F_o.DataInfo.Units = 'N';
    F_F_o.TimeInfo.StartDate = startDate;

    F_T_o = timeseries([data{6} data{7} data{8}], timestamps, 'Name', ...
        'Torque in F at origin');
    F_T_o.DataInfo.Units = 'Nm';
    F_T_o.TimeInfo.StartDate = startDate;
    
    LogTable{tRow, 'F_F_o'} = F_F_o;
    LogTable{tRow, 'F_T_o'} = F_T_o;
end

% Set description
LogTable.Properties.VariableDescriptions(4:5) = ...
    {'Force in F frame at origin of F frame, [x y z]', ...
    'Torque in F frame at origin of F frame, [x y z]'};

%    'Torque in F frame at origin of F frame, [x y z]'};


%% Save to matfile

targetFile = 'data/datasetRAW.mat';

if exist(targetFile, 'file') == 0     % file does not exist jet
    save(targetFile, 'LogTable');
else
    answer = questdlg( ...
        sprintf('File %s already exists. Do you want to overwrite LogTable?', targetFile), ...
        'Overwrite file?');
    
    if strcmpi(answer, 'yes')
        save(targetFile, '-append', 'LogTable');
    else
        % Do not save, just quit.
    end
end

