% Visualize a mesh of the moving spear and frames in Rviz

% Connect to ROS network and initialize global node
rosinit();

% Start rviz from bash
%roslaunch spear_visualization rviz_spear.launch 


%% Load data and transforms:
if ~exist('DATA', 'var')
    DATA = load('data/dataset04.mat');
end

data = outerjoin(DATA.TRAJ, DATA.PARA, 'keys', 'ExpName', ...
    'RightVariables', ...
    {'ImpactID', 'Target', 'Modus', 'Participant'});


W_ov_U = data{12, 'W_ov_U'}.gettsbeforeatevent(data{12,'Impact'});
Q_WU = data{12, 'Q_WU'}.gettsbeforeatevent(data{12,'Impact'});

U_ov_C = data{12, 'U_ov_C'};
Q_UC = data{12, 'Q_UC'};

assert(isequal(W_ov_U.Time, Q_WU.Time));


% Load frame definition
run data/frames.m

% Spear reference frame horizontal (Sh): Aligned with 
% the reference tag frame/ world frame, but swapped z and x axis.
A_ShW = [0 0 1; 0 1 0; -1 0 0];

% Spear reference frame vertical (Sh): Aligned with 
% the reference tag frame/ world frame, but rotated about pi around y
A_SvW = [-1 0 0; 0 1 0; 0 0 -1];

% Spear reference frame (S): Aligned with the reference tag frame, but
% swapped z and x axis. Used for measuring impact angels.

tfmsg = appendTransform(timeseries, 'world', 'U', W_ov_U, Q_WU);


tfmsg = appendTransform(tfmsg, 'U', 'camera_rcars', U_ov_C, Q_UC);
tfmsg = appendTransform(tfmsg, 'U', 'tail', U_ov_L, rotm2quat(A_UL));
tfmsg = appendTransform(tfmsg, 'tail', 'T', L_ov_T, rotm2quat(inv(A_TL)));
tfmsg = appendTransform(tfmsg, 'tail', 'F', L_ov_F, rotm2quat(inv(A_FL)));
tfmsg = appendTransform(tfmsg, 'tail', 'C', L_ov_C, rotm2quat(inv(A_CL)));
tfmsg = appendTransform(tfmsg, 'world', 'Sh', [0 0 0], rotm2quat(inv(A_ShW)));
tfmsg = appendTransform(tfmsg, 'world', 'Sv', [0 0 0], rotm2quat(inv(A_SvW)));


%% Write Pose data to ROS messages

playTFSeries(tfmsg, 0.5);



%% Return angles

rotm2eul(A_SW*inv(ori)*quat2rotm(data.Q_UC)*A_CL);


