%testTftree Send a trajectory as ros /tf object into the ros network for
%visualization with eg. RViz.

rosinit();


%%
% Create tf topic for publishing transformations
tfpub = rospublisher('/tf', 'tf2_msgs/TFMessage');

% tftree = rostf



%% Build ROS transform message

tfmsg1 = rosmessage('tf2_msgs/TFMessage');
tfmsg1.Transforms = rosmessage('geometry_msgs/TransformStamped');

tfmsg1.Transforms(1).Header.Stamp = rostime('now');

tfmsg1.Transforms(1).ChildFrameId = 'wheel';
tfmsg1.Transforms(1).Header.FrameId = 'world';


%% Start rotating
flag = true;
quatrot = axang2quat([0 1 0 0]);

while flag
    
    quatrot = quatmultiply(quatrot, axang2quat([0 1 0 deg2rad(30)]));

    tfmsg1.Transforms(1).Transform.Translation.X = 0;
    tfmsg1.Transforms(1).Transform.Translation.Y = 1;
    tfmsg1.Transforms(1).Transform.Translation.Z = 1.5;

    tfmsg1.Transforms(1).Transform.Rotation.W = quatrot(1);
    tfmsg1.Transforms(1).Transform.Rotation.X = quatrot(2);
    tfmsg1.Transforms(1).Transform.Rotation.Y = quatrot(3);
    tfmsg1.Transforms(1).Transform.Rotation.Z = quatrot(4);

    send(tfpub, tfmsg1);

    pause(0.1);
end






%%
% tfStampedMsg = rosmessage('geometry_msgs/TransformStamped');
% tfStampedMsg.ChildFrameId = 'wheel';
% tfStampedMsg.Header.FrameId = 'world';
% 
% tfStampedMsg.Transform.Translation.X = 0;
% tfStampedMsg.Transform.Translation.Y = -0.2;
% tfStampedMsg.Transform.Translation.Z = -0.3;
% 
% quatrot = axang2quat([0 1 0 deg2rad(30)])
% tfStampedMsg.Transform.Rotation.W = quatrot(1);
% tfStampedMsg.Transform.Rotation.X = quatrot(2);
% tfStampedMsg.Transform.Rotation.Y = quatrot(3);
% tfStampedMsg.Transform.Rotation.Z = quatrot(4);
% 
% tfStampedMsg.Header.Stamp = rostime('now');
% 
% sendTransform(tftree, tfStampedMsg)
% 
% tftree.AvailableFrames

