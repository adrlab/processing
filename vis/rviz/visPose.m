% Visualize trajectories in Rviz

% Connect to ROS network and initialize global node
rosinit();

%% Set-up ROS
tfpub = rospublisher('/tf', 'tf2_msgs/TFMessage');

tfmsg1 = rosmessage('tf2_msgs/TFMessage');
tfmsg1.Transforms = rosmessage('geometry_msgs/TransformStamped');

trafoMsg = rosmessage('geometry_msgs/TransformStamped');
trafoMsg.ChildFrameId = 'foo';
trafoMsg.Header.FrameId = 'world';

tfTree = rostf;

% Create tf topic for publishing poses
poseCamera = rospublisher('/pose_cam', 'geometry_msgs/PoseStamped');

% Start RViz
% ! rosrun rviz rviz &

%% Load dataset:
if ~exist('DATA', 'var')
    DATA = load('data/dataset04.mat');
end

data = outerjoin(DATA.TRAJ, DATA.PARA, 'keys', 'ExpName', ...
    'RightVariables', ...
    {'ImpactID', 'Target', 'Modus', 'Participant'});

data = data(data.ExpName == 'BP107_3', :);
pos = data{1, 'W_ov_U'};
ori = data{1, 'Q_UW'};


%% Write Pose data to ROS messages

% Scale time vector of timeseries
pauseTimes = 1 * diff(pos.Time);

while true
for i=1:pos.Length
    
    trafoMsg.Header.Stamp = rostime('now');
    tfmsg1.Transforms = trafoMsg;
    send(tfpub, tfmsg1);

    % publish all poses in a row
    send(poseCamera, createPoseMsg(pos.Data(i,:), ori.Data(i,:)));
    
    
    if i < pos.Length
        pause(pauseTimes(i));
    end
end
end


