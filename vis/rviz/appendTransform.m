function [ tfmsgOut ] = appendTransform( tfmsgIn, sourceFrame, targetFrame, trans, rot)
%appendTransform Append transform (translation and rotation) to TFMessage object.
%   INPUT
%   tfmsg:          Timeseries of TFMessage objects
%   sourceFrame:    Source frame of the transform
%   targetFrame:    Target frame of the transform
%   trans:          Position vector expressed in source frame pointing from
%                   source origin to the origin of the target frame. I.e.
%                   add trans to a position vector in target frame and you
%                   receive the position expressed in source frame.
%                   Vector, as [x y z].
%   rot:            Rotation, which transforms objects expressed in target
%                   frame into source frame, e.g. R_ST: from T to S. This
%                   corresponds to the ros convention of TransformStamped.
%                   Quaternion, as [w x y z],
%
%   trans and rot can be either given as constant double vector in which case
%   the constant transform is appended to every sample in tfmsgIn.
%   If the transform is given as timeseries objects, tfmsgIn is resampled
%   at every timestamp of the united timevector.


if isa(trans, 'timeseries') && isa(rot, 'timeseries')
    % Initialize transform with timeseries pos and ori
    assert(isempty(tfmsgIn.Time), 'tfmsgIn must be empty when parsing pose as timeseries');    
    assert(isequal(trans.Time, rot.Time), 'pos and ori are not synchronized');
    
    tfmsgIn.Name = sprintf('Trafo from %s to %s', sourceFrame, targetFrame);
    tfmsgIn.Time = trans.Time;
    trafos = robotics.ros.msggen.tf2_msgs.TFMessage.empty(0, trans.Length);
    
    for j=1:trans.Length
        trafos(j) = rosmessage('tf2_msgs/TFMessage');
        trafos(j).Transforms = buildTS(trans.Data(j,:), rot.Data(j,:));
    end
    
    %'Data', 12, ... %
    tfmsgIn.Data = trafos';
    
    
elseif isnumeric(trans) && isnumeric(rot)
    % Add constant transform to every sample of tfmsgIn
    assert(~isempty(tfmsgIn.Time), 'tfmsgIn must be non-empty when parsing constant pose');
    
    trafoConst = buildTS(trans, rot);
    
    for j=1:tfmsgIn.Length
        tfmsgIn.Data(j).Transforms(end+1) = trafoConst;
    end
else
    error('pos and ori must be either timeseries or numeric vectors')
end

tfmsgOut = tfmsgIn;


function tsMsg = buildTS( posvec, orivec )
    % Take pos and ori and return a ros TransformStamped message.
    % 
    % sourceFrame = 'world';
    % targetFrame = 'rcars_inertial';

    tsMsg = rosmessage('geometry_msgs/TransformStamped');

    % trafo .Header.Stamp = rostime('now');

    tsMsg.ChildFrameId      = targetFrame;
    tsMsg.Header.FrameId    = sourceFrame;

    tsMsg.Transform.Translation.X = posvec(1);
    tsMsg.Transform.Translation.Y = posvec(2);
    tsMsg.Transform.Translation.Z = posvec(3);

    tsMsg.Transform.Rotation.W = orivec(1);
    tsMsg.Transform.Rotation.X = orivec(2);
    tsMsg.Transform.Rotation.Y = orivec(3);
    tsMsg.Transform.Rotation.Z = orivec(4);
end

end

