%testTftree Send a trajectory as ros /tf object into the ros network for
%visualization with eg. RViz.

rosinit();


%%
% Create tf topic for publishing transformations
posepub = rospublisher('/mat_poses', 'geometry_msgs/PoseStamped');

% tftree = rostf



%% Build ROS transform message

posemsg1 = rosmessage('geometry_msgs/PoseStamped');

posemsg1.Header.Stamp = rostime('now');
posemsg1.Header.FrameId = 'world';


%% Start rotating
flag = true;
quatrot = axang2quat([0 1 0 0]);

while flag
    
    quatrot = quatmultiply(quatrot, axang2quat([0 1 0 deg2rad(30)]));

    posemsg1.Pose.Position.X = 0;
    posemsg1.Pose.Position.Y = 1;
    posemsg1.Pose.Position.Z = 1.5;

    posemsg1.Pose.Orientation.W = quatrot(1);
    posemsg1.Pose.Orientation.X = quatrot(2);
    posemsg1.Pose.Orientation.Y = quatrot(3);
    posemsg1.Pose.Orientation.Z = quatrot(4);

    send(posepub, posemsg1);

    pause(0.2);
end




%% Use createPoseMsg()

flag = true;
quatrot = axang2quat([0 1 0 0]);

while flag
    
    % Recursively apply rotation
    quatrot = quatmultiply(quatrot, axang2quat([0 1 0 deg2rad(30)]));

    send(posepub, createPoseMsg([0 3 3], quatrot));
    
    pause(0.2);
end



