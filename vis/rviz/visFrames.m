% Visualize moving spear frames in Rviz

% Connect to ROS network and initialize global node
rosinit();


%% Load data and transforms:
if ~exist('DATA', 'var')
    DATA = load('data/dataset04.mat');
end

data = outerjoin(DATA.TRAJ, DATA.PARA, 'keys', 'ExpName', ...
    'RightVariables', ...
    {'ImpactID', 'Target', 'Modus', 'Participant'});

data = data(1, :);

W_ov_U = data{1, 'W_ov_U'};
Q_WU = data{1, 'Q_WU'};

assert(isequal(W_ov_U.Time, Q_WU.Time));

% Load frame definition
run data/frames.m

% Spear reference frame (S): Aligned with the reference tag frame, but
% swapped z and x axis. Used for measuring impact angels.
A_SW = [0 0 1; 0 1 0; -1 0 0];

tfmsg = appendTransform(timeseries, 'world', 'IMU', W_ov_U, Q_WU);
tfmsg = appendTransform(tfmsg, 'world', 'S', [0 0 0], rotm2quat(inv(A_SW)));
tfmsg = appendTransform(tfmsg, 'IMU', 'C', data.U_ov_C, data.Q_UC); 

tfmsg = appendTransform(tfmsg, 'C', 'L', -(A_CL * L_ov_C')', rotm2quat(A_CL)); 
tfmsg = appendTransform(tfmsg, 'L', 'T', L_ov_T, rotm2quat(inv(A_TL))); 
tfmsg = appendTransform(tfmsg, 'L', 'F', L_ov_F, rotm2quat(inv(A_FL))); 


%% Write Pose data to ROS messages

playTFSeries(tfmsg, 0.5);



%% Return angles

rotm2eul(A_SW*inv(ori)*quat2rotm(data.Q_UC)*A_CL);


