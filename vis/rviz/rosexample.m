%rosexample Use ROS to visualize a moving arrow in RViZ.

% Connect to ROS network and initialize global node
rosinit

% Start RViz package
% system('rosrun rviz rviz');

% Create a node to publish trajectory data
matdat = robotics.ros.Node('matdat', 'localhost');

matdatPub = robotics.ros.Publisher(matdat, '/mat_traj', 'geometry_msgs/PoseStamped');

% Create a message and insert a pose
posemsg = rosmessage(matdatPub);

posemsg.Header.Seq = 0;
posemsg.Header.Stamp.Sec = 0;
posemsg.Header.Stamp.Nsec = 0;
posemsg.Header.FrameId = '/CFrame';

posemsg.Pose.Position.X = 1;
posemsg.Pose.Position.Y = 2;
posemsg.Pose.Position.Z = 0;

posemsg.Pose.Orientation.X = 0.2;
posemsg.Pose.Orientation.Y = 1;
posemsg.Pose.Orientation.Z = 1;
posemsg.Pose.Orientation.W = 1;


%% Send out the message
send(matdatPub, posemsg);


%% Modify pose and send iteratively
for i=1:10
    posemsg.Header.Seq = i;
    posemsg.Header.Stamp.Sec = i;
    
    posemsg.Pose.Position.Z = i/10;
    
    posemsg.Pose.Orientation.X = i;
    
    send(matdatPub, posemsg);
    
    pause(0.5);
end



%% Set-up simple ROS network (modified MATLAB example)
% Initialize three nodes (in addition to the global MATLAB node that is 
% created through rosinit). Note that the nodes will try to connect to the
% ROS master at 'localhost'. If you are connecting to an external master,
% you will have to use its IP address or hostname.
masterHost = 'localhost';
node_1 = robotics.ros.Node('node_1', masterHost);
node_2 = robotics.ros.Node('node_2', masterHost);
node_3 = robotics.ros.Node('node_3', masterHost);

% Create a publisher and subscriber for the '/pose' topic
twistPub = robotics.ros.Publisher(node_1,'/pose','geometry_msgs/Twist');
twistPubmsg = rosmessage(twistPub);
twistSub = robotics.ros.Subscriber(node_2,'/pose');

% Create publishers and subscribers for the '/scan' topic
scanPub = robotics.ros.Publisher(node_3,'/scan','sensor_msgs/LaserScan');
scanPubmsg = exampleHelperROSLoadRanges();
scanSub1 = robotics.ros.Subscriber(node_1,'/scan');
scanSub2 = robotics.ros.Subscriber(node_2,'/scan');

% Create two service servers for the '/add' and '/reply' services
srv1 = robotics.ros.ServiceServer(node_3,'/add', 'roscpp_tutorials/TwoInts');
srv2 = robotics.ros.ServiceServer(node_3,'/reply', 'std_srvs/Empty', @exampleHelperROSEmptyCallback);

% Load sample data for inspecting messages
tffile = fullfile(fileparts(mfilename('fullpath')), '..', 'data', 'tfdata.mat');
tfcell = load(tffile);
tf = tfcell.t;
clear tffile
clear tfcell

% Create a timer for publishing messages and assign appropriate handles
% The timer will call exampleHelperROSSimTimer at a rate of 10 Hz.
timerHandles.twistPub = twistPub;
timerHandles.twistPubmsg = twistPubmsg;
timerHandles.scanPub = scanPub;
timerHandles.scanPubmsg = scanPubmsg;
simTimer = ExampleHelperROSTimer(0.1, {@exampleHelperROSSimTimer,timerHandles});

rosservice list

rosservice info /add

