function [ ] = playTFSeries( tfmsg, speed )
%playTFSeries Playback a timeseries of tf messages
%   Detailed explanation goes here


tfpub = rospublisher('/tf', 'tf2_msgs/TFMessage');

pauseTimes = 1/speed * diff(tfmsg.Time);

fprintf('Playing tf timeseries ...');

for i=1:tfmsg.Length
    
    send(tfpub, tfmsg.Data(i));  

    
    if i < tfmsg.Length
        pause(pauseTimes(i));
    end
end

fprintf(' Done\n');



end

