function [ posemsgs ] = timeSeries2poseMsgs( trajectory )
%timeSeries2poseMsgs Convert timeseries object to ROS pose messages.
%   Detailed explanation goes here

% Check for NaNs
isOK = ~(isnan(trajectory.timestamps) | ...
    isnan(trajectory.x) | ...
    isnan(trajectory.y) | ...
    isnan(trajectory.z) | ...
    isnan(trajectory.qx) | ...
    isnan(trajectory.qy) | ...
    isnan(trajectory.qz) | ...
    isnan(trajectory.qw));

timestamps = trajectory.timestamps(isOK);
x = trajectory.x(isOK);
y = trajectory.y(isOK);
z = trajectory.z(isOK);
qx = trajectory.qx(isOK);
qy = trajectory.qy(isOK);
qz = trajectory.qz(isOK);
qw = trajectory.qw(isOK);

numStamps = length(timestamps);

posemsgs = robotics.ros.msggen.geometry_msgs.PoseStamped.empty(0, numStamps);

for i=1:numStamps
                   
    posemsgs(i) = rosmessage(rostype.geometry_msgs_PoseStamped);

    sec = floor(timestamps(i));
    nsec = round((timestamps(i)-sec)*1e+9);

    posemsgs(i).Header.Seq = i;
    posemsgs(i).Header.Stamp.Sec = sec;
    posemsgs(i).Header.Stamp.Nsec = nsec;

    posemsgs(i).Pose.Position.X = x(i);
    posemsgs(i).Pose.Position.Y = y(i);
    posemsgs(i).Pose.Position.Z = z(i);

    posemsgs(i).Pose.Orientation.X = qx(i);
    posemsgs(i).Pose.Orientation.Y = qy(i);
    posemsgs(i).Pose.Orientation.Z = qz(i);
    posemsgs(i).Pose.Orientation.W = qw(i);
end

end

