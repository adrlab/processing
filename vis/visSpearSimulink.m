% Vizualize CAD mesh of the spear in SimMechanics and render video.
% Run the Simulink model HighTecSpear.slx from here.
% For correct video rendering the mechanics explorer must be open and the
% desired camera view must be set-up before the rendering process is
% started. 

clear all;

modelName = 'HighTecSpear';

open(['model/' modelName '.slx']);

load data/dataset05.mat;
run data/frames.m;

traj = innerjoin(TRAJ, PARA, 'keys', 'ExpName', 'Rightvariables', 'ImpactID');

traj = traj(~any(ismissing(traj), 2), :);


%% Set-up Mechanics Explorer
% Run one simulation to start the Mechanics Explorer. Once it is open
% set camera angle and zoom manually to be ready for the video rendering.

ori = traj.Q_WU(1).gettsbeforeevent(KinTable.EndOfMotion(1));
pos = traj.W_ov_U(1).gettsbeforeevent(KinTable.EndOfMotion(1));

sim(modelName, 'SaveFormat', 'StructureWithTime');



%% Render videos

fprintf(fopen('vis/out/Note.txt', 'w+'), ...
    ['Video playback speed is 0.5x\n', ...
    'Spear motions are cropped to the phase before the impact event']);
fclose('all');

for i = 75:height(traj)
    fprintf('%s, ID: %u, i = %u\n', traj.ExpName(i), traj.ImpactID(i), i);
    
    % Limittime series to interval before 'end of motion'
    ori = traj.Q_WU(i).gettsbeforeevent(KinTable.EndOfMotion(i));
    pos = traj.W_ov_U(i).gettsbeforeevent(KinTable.EndOfMotion(i));

   
    if all(all(ismissing(ori.Data))) || all(all(ismissing(pos.Data)))
        continue;
    end
    
    simOut = sim(modelName, 'SaveFormat', 'StructureWithTime');
    
    smwritevideo(modelName, ...
        fullfile('vis/out', char(num2str(traj{i, 'ImpactID'}) + "_" + traj{i, 'ExpName'} + ".avi")), ...
        'PlaybackSpeedRatio',0.5,'FrameRate',30);
    
    pause(10);  % Make sure the video rendering is finished begfore next sim starts
    

end



