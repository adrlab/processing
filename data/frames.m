%frames.m Definition of coordinate frames extracted from the CAD model 
% All frames are right handed, expressed relative to the absolute CSYS of
% the CAD drawing called tail frame, which is located at the tail of the shaft. 
% The z axis of the tail frame points alongside the shaft, the x axis
% points upwards. 
%
% Most important frames are the sensor frames (camera, IMU and force) and the
% tip frame, which is used describe spear trajectories in the 
% results.
%
% Transformations are described using roatation matrices and position
% vectors from frame origin to frame origin in meters.
%
% Naming convention
% L: tail frame
% C: camera frame
% U: IMU frame
% F: force frame
% P: clamp frame
% T: tip frame

mm2m = 1e-3;

% Camera frame
X =   49.000000000;
Y =    0.988315674;      
Z = 1539.060214450;     
L_ov_C = [X Y Z] * mm2m;
A_CL = [1 0 0; 0 0 -1; 0 1 0];             


% IMU Frame
X =   -8.540707856;
Y =   37.792939940;      
Z = 1541.236408802;
L_ov_U = [X Y Z]* mm2m;
A_UL = [0.866025404  0.5 0; 0 0 -1; -0.5 0.866025404 0];


% Force frame
X =    0.000000000;
Y =    0.000000000;      
Z = 1575.580000001;
L_ov_F = [X Y Z] * mm2m;
A_FL = [-1 0 0; 0 1 0; 0 0 -1];


% Clamp frame
X =    0.000000000;
Y =   -0.000000000;
Z = 1699.498847554;
L_ov_P = [X Y Z] * mm2m;
A_PL = [1 0 0; 0 1 0; 0 0 1];


% Tip frame
X =    0.000000000;
Y =    0.000000000;      
Z = 1839.498847554;      
L_ov_T = [X Y Z] * mm2m;
A_TL = [1 0 0; 0 1 0; 0 0 1];


% Comupte IMU to tail translation
U_ov_L = (A_UL * (- L_ov_U )' )';

% Remove tmp variables
clear X Y Z mm2m


